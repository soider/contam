VERSION="0.3"

build_deps:
	echo "Will install all build dependencies"
	apt-get install python python-dev python-pip libxslt1.1 libxslt1-dev libcurl4-gnutls-dev libcurl3 python-virtualenv libevent-dev dpkg debconf debhelper lintian

deb:
	echo "Making deb"
	mkdir -p deploy/contaminant/usr/local/contaminant;
	mkdir -p dist;
	mkdir -p deploy/contaminant/usr/local/bin;
	virtualenv deploy/env;
	deploy/env/bin/pip install lxml;
	deploy/env/bin/python setup.py install;
	rm -rf `find . -name '*.pyc'`
	sed -ie "s/^VIRTUAL_ENV=.*/VIRTUAL_ENV=\/usr\/local\/contaminant\/env/g" deploy/env/bin/*
	sed -ie "s/\#\!.*\/bin\/python/\#\!\/usr\/local\/contaminant\/env\/bin\/python/g" deploy/env/bin/*
	sed -ie "s/^Version: .*/Version: ${VERSION}/g" deploy/contaminant/DEBIAN/control
	cp -r deploy/env deploy/contaminant/usr/local/contaminant;
	#cp -r deploy/env/lib/python2.7/site-packages deploy/contaminant/usr/local/contaminant/site-packages
	cp -r bin deploy/contaminant/usr/local;

	cd deploy && fakeroot dpkg-deb --build contaminant;
	mv deploy/contaminant.deb dist/contaminant-${VERSION}.deb;


clean_deploy:

	rm -rf deploy/contaminant/usr