Contaminant
===========

# Установка:

1) Установка mongodb-10gen
    $ sudo apt-key adv --keyserver keyserver.ubuntu.com --recv 7F0CEB10
    $ echo 'deb http://downloads-distro.mongodb.org/repo/debian-sysvinit dist 10gen' | sudo tee /etc/apt/sources.list.d/10gen.list
    $ sudo apt-get update
    $ sudo apt-get install mongodb-10gen

2) Сборка debian пакета (должна происходить на точно такой же операционной системе, как на целевом сервере)
    $ sudo make build_deps  # установка сборочных зависимостей
    $ make deb # сборка пакета
    $ git checkout deploy # очистка изменений

В dist/contaminant-<version>.deb лежит результат

3) Установка зависимостей для deb-пакета
    $ sudo apt-get install mongodb-10gen python python-dev python-pip libxslt1.1 libxslt1-dev rabbitmq-server libcurl4-gnutls-dev libcurl3 curl supervisor python-virtualenv libevent-dev python-celery

4) Установка deb пакета
    $ sudo dpkg -i dist/contaminant-<version>.deb

После первой установки выполнить команду
    contaminant_after_install (создаст темплейты в бд)

Конфиги лежат в
    /etc/contaminant.d/user_conf.py
    /etc/supervisor/conf.d/contaminant.conf

Создание администратора:
    $ contaminant_cli add-admin <email> <password>

Проверка пользователя:
    $ curl 127.0.0.1:5000/login -X POST -d "login=<email>" -d "password=<password>"
    {"result": {"status": 200, "answer": "<token>"}}

    $ curl 127.0.0.1:5000/templates -H "X-Contaminant-Login: <email>" -H "X-Contaminant-Auth: <token>"

    {"result": {"status": 200, "answer": [{"name": "PhpBBBasictemplate", "id": "5194aa9b6e95523402bf698f"}, {"name": "InvisionBasicTemplate", "id": "5194aa9b6e95523402bf6990"}, {"name": "VBulletin4Basic", "id": "5194aa9b6e95523402bf6991"}]}}


========

# Общее описание

## Используемые термины

### template, шаблон
документ в коллекции contaminant.template, описание модели в contaminant.data.models.profile.Template
"Болванка", на основе которой пользователи создают свои профили.
Состоит из имени, класса-постера, который используется для создания тредов и постов, и json-конфига.
После установки есть три стандартных темплейта, можно посмотреть в монге.

Описание конфигов для профилей форумов:

        "login_input_xpath" - xpath-селектор для выбора инпута ввода логина
        "password_input_xpath" - xpath-селектор для выбора инпута ввода пароля
        "login_form" - словарь описывающий выборку форму для логина вида {"id": "#dom_id", "name": "formname"}. (см пример существующих темплейтов)
        "post_form"  - аналогично
        "thread_form" - аналогично
        "forum_link_xpath_format" - селектор для поиска ссылки на форум по названию
        "new_thread_wait_on_page_after" - время которое необходимо подождать после загрузки страницы с формой создания треда
        "new_thread_link_xpath" - селектор для поиска ссылки на страницу создания треда
        "new_thread_text_input_xpath" - селектор для поиска инпута с текстом
        "new_thread_subject_input_xpath" - селектор для поиска инпута с сабжем
        "new_thread_error_message_xpath" - селектор для поиска ошибки после сабмита формы
        "new_post_link_xpath" - селектор для поиска ссылки на страницу создания поста в треде
        "new_post_text_input_xpath" - аналогично тредам
        "new_post_error_message_xpath" - - аналогично тредам
        "new_post_submit_args" - выбор кнопки для сабмита формы (если несколько кнопок), см пример предустановленных темплейтов
        "new_thread_submit_args" - аналогично для создания треда

### profile
связка пользователя, шаблона и данных авторизации
Используется для создания тасков-заданий

### task
задача создаваемая через рэббит на создание треда или поста

## Пример работы

Авторизация
$ curl 127.0.0.1:5000/login -X POST -d "login=sahnov.m@gmail.com" -d "password=spb43afk"
{"result": {"status": 200, "answer": "7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a"}}%

Список моих профилей
curl 127.0.0.1:5000/profiles -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a"
{"result": {"status": 200, "answer": [{"login_url": "http://192.168.1.71/forum/ucp.php?mode=login", "url": "http://192.168.1.71/forum/", "template": "517f99c5fe01d620439bcb3a", "date_created": "Thu, 02 May 2013 14:00:05 -0000", "login": "soider", "password": "spb43afk", "id": "51823925fe01d6972f8d3474"}]}}%

(По адресу http://192.168.1.71/forum) установлен обычный phpbb.


Создание нового треда.
    $ curl 127.0.0.1:5000/tasks -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a" -X POST -d "action=create_thread" -d "profile=51823925fe01d6972f8d3474" -d "text=%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%2C%20%D0%BC%D0%B8%D1%80%21%20%D0%AD%D1%82%D0%BE%20%D1%82%D0%B5%D0%BA%D1%81%D1%82%20%D1%81%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D1%8F%2C%20%D0%BE%D0%BD%20%D0%B4%D0%BB%D0%B8%D0%BD%D0%BD%D1%8B%D0%B9" -d "subject=%D0%9F%D1%80%D0%B8%D0%B2%D0%B5%D1%82%2C%20%D0%BC%D0%B8%D1%80%21%20%D0%AD%D1%82%D0%BE%20%D0%B7%D0%B0%D0%B3%D0%BE%D0%BB%D0%BE%D0%B2%D0%BE%D0%BA%20%D1%81%D0%B0%D0%B1%D0%B6%D0%B0%20%D0%B5%D0%BC%D1%83%2015%20%D1%81%D0%B8%D0%BC%D0%B2%D0%BE%D0%BB%D0%BE%D0%B2" -d "forum=Your%20first%20forum"

    {"result": {"status": 200, "answer": {"id": "51976fbafe01d63f416d8b8e"}}}%


    $ curl 127.0.0.1:5000/tasks/51976fbafe01d63f416d8b8e -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a"

    {"result": {"status": 200, "answer": {"status": "SUCCESS", "id": "51976fbafe01d63f416d8b8e", "result": "http://192.168.1.71/forum/viewtopic.php?f=2&t=50"}}}%

    $ curl 127.0.0.1:5000/tasks -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a" -X POST -d "action=create_post" -d "profile=51823925fe01d6972f8d3474" -d "text=%D0%A1%D0%BE%D0%BE%D0%B1%D1%89%D0%B5%D0%BD%D0%B8%D0%B5%20%D0%B2%20%D1%80%D0%B0%D0%BD%D0%B5%D0%B5%20%D1%81%D0%BE%D0%B7%D0%B4%D0%B0%D0%BD%D0%BD%D1%83%D1%8E%20%D1%82%D0%B5%D0%BC%D1%83%20%D0%B4%D0%BB%D1%8F%20%D0%BF%D1%80%D0%BE%D0%B2%D0%B5%D1%80%D0%BA%D0%B8" -d "thread=http%3A//192.168.1.71/forum/viewtopic.php%3Ff%3D2%26t%3D50"

    {"result": {"status": 200, "answer": {"id": "5197704ffe01d63f416d8b8f"}}}

    $ curl 127.0.0.1:5000/tasks/5197704ffe01d63f416d8b8f -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a"

    {"result": {"status": 200, "answer": {"status": "SUCCESS", "id": "5197704ffe01d63f416d8b8f", "result": "http://192.168.1.71/forum/viewtopic.php?f=2&t=50"}}}

    $ curl 127.0.0.1:5000/tasks -H "X-Contaminant-Login: sahnov.m@gmail.com" -H "X-Contaminant-Auth: 7afcdff9-94e2-45df-b158-eaa7ca406c782e746f7804e2613b41b47070e6fec10a"

    {"result": {"status": 200, "answer": [{"status": "SUCCESS", "id": "51976fbafe01d63f416d8b8e", "result": "http://192.168.1.71/forum/viewtopic.php?f=2&t=50"}, {"status": "SUCCESS", "id": "5197704ffe01d63f416d8b8f", "result": "http://192.168.1.71/forum/viewtopic.php?f=2&t=50"}]}}%

========

# Описание api:

Роутинг url'ов задается в файле contaminant/web/urls.py в формате
    [
        ("module", "class", "/url")
    ]

Описания параметров можно посмотреть у соответствующего класса в полях <METHOD_NAME>_PARAMETERS

Для доступа к закрытым ресурсам запрос должен иметь хедеры X-Contaminant-Login: <email> и X-Contaminant-Auth: <токен>

    ("register", "Registration", "/register")
    POST запрос создает пользователя
    Параметры:
    login - должен быть корректный email
    password - пароль

    ("register", "Activation", "/activate")
    POST запрос активирует пользователя с соответствующим  activate_hash
    hash - строка

    ("register", "Resend", "/activate/resend")
    POST запрос повторно отправляет письмо с ссылкой активации
    login - почта пользователя


    ("authorize", "Authorize", "/login")
    POST запрос возвращает токен для подписи запросов
    login, password

    ("profiles", "Template", "/templates")
    Закрытый ресурс (запрос должен быть подписан)
    GET запрос возвращает список темплейтов

    ("profiles", "Profile", "/profiles"),
    GET Получить список профилей пользователя
    POST - создать профиль
    Параметры:
        login - логин пользователя
        password - пароль пользователя
        name - строка название
        url - главная страница форума (урл должен кончаться на /)
        login_url - страница с формой логина
        template - id темплейта

    ("tasks", "TaskCreator", "/tasks")
    GET - получить список текущих тасков (статус и результат)
    POST - создать таск
    Параметры:
        action = (create_post | create_thread)
        profile = id профиля
        text = urlencode текст сообщения
        subject = urlencode заголовок треда (нужен только при action = create_thread)
        thread = urlencode ссылка на тред (нужен только при action = create_post)
        forum =  urlencode название форума (нужен только при action = create_thread), просто текст с названием форума, используется в xpath-селекторе.


    ("admin", "TemplateAdmin", "/admin/templates")
    POST запрос доступен только администратору (иначе 404), создает новый темплейт
        name - строка, название темплейта
        board_class - класс, который будет использоваться этим темплейтом, сейчас это выбор из Vbulletin4Board, InvisionDefaultBoard, PhpBoard (см ниже)
        config - urlencode json строка, конфиг для темплейты (см ниже.)

