DEBUG = False

ROOT_URL = "http://127.0.0.1/client"
API_URL = "http://127.0.0.1:5000"

AUTO_CONNECT = True

MONGO_CONNECTION = {
    "db": "contaminant",
    "host": "127.0.0.1",
    "port": 27017,
}

SECRET = "b072d20079919840b6f0fb3bdaf98c71"

FROM_EMAIL = "contaminant@contaminant.local"

SMTP_HOST = "localhost"
SMTP_PORT = 25
SMTP_AUTH = False
STMP_USER = "login"
SMTP_PASS = "password"

BROKER_URL = 'amqp://guest@localhost//'

CELERY_RESULT_BACKEND = "mongodb"

CELERY_MONGODB_BACKEND_SETTINGS = {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "contaminant_celery",
    "taskmeta_collection": "tasks_results",
}

CELERY_TIMEZONE = 'Europe/Moscow'
CELERY_ENABLE_UTC = True

CELERY_CHORD_PROPAGATES = True
