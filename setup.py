from setuptools import setup, find_packages

print find_packages()

setup(
    name='contaminant',
    version='0.3',
    packages=find_packages(),
    url='https://github.com/anpak1/contaminant/',
    license='BSD',
    author='Michael Sahnov',
    author_email='sahnov.m@gmail.com',
    description='',
    entry_points={
        'console_scripts': [
            "contaminant_cli=contaminant.cli.tools:main",
            "contaminant_after_install=contaminant.cli.install:main"
        ]
    },
    install_requires=['grab',
                      'lxml',
                      'selenium',
                      'pycurl',
                      'cssselect',
                      'flower',
                      'pymongo',
                      'flask',
                      'flask-restful',
                      'mongoengine',
                      'blinker',
                      'gevent',
                      'gunicorn',
                      'jinja2']
)
