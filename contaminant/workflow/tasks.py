# *-* coding: utf-8 *-*

from __future__ import absolute_import

__author__ = 'soider'

import logging
import urlparse

from bson.objectid import ObjectId

from contaminant import conf

from contaminant.core.boards import get_board_class
from contaminant.core.sessions import get_board_session
from contaminant.workflow.broker import celery
from contaminant.data.models.cookie import Cookie
from contaminant.templates import get_template

from celery.utils.mail import Mailer, Message


def create_task(task_name, **kwargs):
    """
        Simple helper to creating tasks from other modules
    """
    return globals()[task_name].delay(**kwargs)


@celery.task
def send_activation_mail(login, hash):
    logger = logging. \
        getLogger("contaminant.workflow.tasks.send_activation_mail")
    logger.debug("Send email to %s with %s", login, hash)
    subject = u"Активация аккаунта"
    body = get_template("mail/register.html").render(
        root_url=conf.ROOT_URL,
        hash=hash
    )
    print body
    create_task("send_mail", to=login, subject=subject, body=body)


@celery.task
def send_mail(to, subject, body, msg=None):
    args = {
        "host": conf.SMTP_HOST
    }
    if conf.SMTP_AUTH:
        args["user"] = conf.STMP_USER
        args["password"] = conf.SMTP_PASS
    if conf.SMTP_PORT:
        args["port"] = conf.SMTP_PORT
    mail = Mailer(
        **args
    )
    if msg is not None:
        message = msg
    else:
        message = Message(
            to=to,
            subject=subject,
            sender=conf.FROM_EMAIL,
            body=body,
            charset='utf-8'
        )
    mail.send(message)


@celery.task
def create_post(thread, text, profile):
    """Inner entry point for create_post task
    :param thread: string link to thread
    :param text: string message body
    :param profile: forum profile
    """
    logger = logging. \
        getLogger("contaminant.workflow.tasks.create_post")
    logger.debug("Begin creating post")
    logger.debug("Post %s", text)
    logger.debug("Thread %s", thread)
    cls_name = profile["board_class"]
    session_cls = get_board_session(cls_name)
    session = session_cls(driver_args={"debug_post": True})
    logging.debug("Try to get cookies from storage")
    domain = urlparse.urlparse(
        profile["config"]["root_url"]
    ).netloc
    cookie = Cookie.objects.find_by_user_and_domain(
        profile["user_id"],
        domain
    )
    if cookie:
        logger.debug("Have predefined cookies")
        cookie = cookie[0]
        logger.debug(cookie.cookies)
        session.set_cookies(cookie.cookies)
        logger.debug("Set predefined cookies")
    else:
        logger.debug("Have no cookie in storage")
    board_class = get_board_class(cls_name)
    board_inst = board_class(
        profile["config"],
        session,
        logger
    )
    board_inst.authorize(
        profile["login"],
        profile["password"]
    )
    board_inst.create_post(thread, text)
    # Explicitly remove logger, because logger object unserializable
    del board_inst.logger
    process_after.delay(board_inst, profile)
    return thread


@celery.task(ignore_result=True)
def process_after(board_inst, profile):
    logger = logging. \
        getLogger("contaminant.workflow.tasks.process_after")
    current_cookies = board_inst.session.response.cookies
    domain = urlparse.urlparse(
        board_inst.session.response.url
    ).netloc
    user = profile["user_id"]
    logger.debug("Saving cookies user %s domain %s", user, domain)
    cookie = Cookie.find_or_create_by_user_and_domain(user, domain)
    cookie.set_user_by_id(user)
    cookie.domain = domain
    cookie.cookies = current_cookies
    cookie.save()
    return board_inst, profile


@celery.task
def create_thread(forum, subject, text, profile):
    logger = logging.getLogger("contaminant.workflow.tasks.create_thread")
    logger.debug("Hello from create thread task")
    logger.debug("%s %s %s %s", forum, subject, text, profile)

    cls_name = profile["board_class"]
    session_cls = get_board_session(cls_name)
    session = session_cls()
    board_class = get_board_class(cls_name)
    board_inst = board_class(
        profile["config"],
        session,
        logger
    )
    board_inst.authorize(
        profile["login"],
        profile["password"]
    )
    result = board_inst.create_thread(forum, subject, text)
    # Explicitly remove logger, because logger object unserializable
    del board_inst.logger
    process_after.delay(board_inst, profile)
    return result


if __name__ == "__main__":
    create_post.delay("thread", "text", "profile")
