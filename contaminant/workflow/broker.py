__author__ = 'soider'

from celery import Celery
import celeryconfig

celery = Celery()
celery.config_from_object('contaminant.workflow.celeryconfig')
