__author__ = 'soider'

import logging
from functools import wraps

from contaminant import conf


def inject_logger(func):
    """Inject logger for callable that has logger kwarg"""
    @wraps(func)
    def inner(*args, **kwargs):
        if "logger" not in kwargs:
            if hasattr(func, "im_class"):
                logger_name = "{0}.{1}.{2}".format(
                    func.__module__,
                    func.im_class.__name__,
                    func.__name__
                )
            else:
                logger_name = "{0}.{1}".format(
                    func.__module__,
                    func.__name__
                )
            if conf.DEBUG:
                my_logger = logging.\
                    getLogger("contaminant.tools.decorators.inject_logger")
                my_logger.debug("Build logger %s", logger_name)
            logger = logging.getLogger(logger_name)
            kwargs["logger"] = logger
        return func(*args, **kwargs)
    return inner
