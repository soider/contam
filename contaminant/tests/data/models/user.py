__author__ = 'soider'

import unittest
import hashlib

from contaminant.data.models import user


class FakeRandom(object):

    @classmethod
    def randint(cls, begin=None, top=None):
        return 4


class FakeTime(object):

    @classmethod
    def time(cls):
        return 4


class FakeLogger(object):

    def __getattr__(self, item):
        def fake_log(*args, **kwargs):
            pass
        return fake_log


class TestUser(unittest.TestCase):

    def setUp(self):
        user.random = FakeRandom
        user.time = FakeTime

    def test_salt(self):
        login = "test@test.ru"
        password = "password"
        need_salt = login * FakeRandom.randint()
        need_salt += password * FakeRandom.randint()
        need_salt += str(FakeTime.time())
        need_salt = hashlib.md5(need_salt).hexdigest()
        have_salt = user.User.create_salt(login, password)
        self.assertEqual(need_salt, have_salt)

    def test_password_hash(self):
        login = "test@test.ru"
        password = "password"
        salt = user.User.create_salt(login, password)
        need_hash = hashlib.md5(
            "".join([salt, login, password, salt])
        ).hexdigest()
        have_hash = user.User.create_password_hash(salt, login, password)
        self.assertEqual(need_hash, have_hash)

    def test_is_valid_password(self):
        login = "test@test.ru"
        password = "password"
        user_object = user.User()
        user_object.login = login
        user_object.salt = user_object.create_salt(login, password)
        user_object.password_hash = user_object.create_password_hash(
            user_object.salt,
            login,
            password
        )
        self.assertEqual(True,
                         user_object.is_valid_password(password))
        user_object.logger = FakeLogger()
        self.assertEqual(False,
                         user_object.is_valid_password("invalidpassword"))


if __name__ == "__main__":
    unittest.main()
