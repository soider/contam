__author__ = 'mihail'


class InvalidAuthorization(Exception):

    def __init__(self, message, response, *args, **kwargs):
        self.response = response
        Exception.__init__(self, message, *args, **kwargs)


class ThreadCreatingError(Exception):
    pass


class PostCreatingError(Exception):
    pass


class InvalidConfig(Exception):
    pass
