__author__ = 'Michael Sahnov'

import logging

from contaminant.core._boards.invision import InvisionDefaultBoard
from contaminant.core._boards.phpbb import PhpBoard
from contaminant.core._boards.vbulletin4Board import Vbulletin4Board
from contaminant.core._boards.js_board import Board as BasicJsBoard
from contaminant.core._boards.nojs_board import Board as BasicNoJsBoard

from contaminant.core.abstract.board import Board

logger = logging.getLogger("contaminant.core.boards")


def get_board_class(board_type):
    try:
        return globals()[board_type]
    except KeyError:
        logger.warning("Can't find class for %s board", board_type)
        logger.warning("Return default")
        return BasicNoJsBoard


def is_valid_board_class_name(class_name):
    if class_name in globals():
        if issubclass(globals()[class_name], Board):
            return True
        else:
            logger.warning("Found class for %s, but not board", class_name)
    return False


__all__ = [
    InvisionDefaultBoard,
    PhpBoard,
    BasicJsBoard,
    BasicNoJsBoard,
    Vbulletin4Board,
    get_board_class,
    is_valid_board_class_name
]
