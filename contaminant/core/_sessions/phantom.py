from __future__ import absolute_import

__author__ = 'mihail'

import selenium
import selenium.webdriver
from contaminant.core.abstract.session import BaseSession
from time import sleep


class FakeResponse(object):
    __slots__ = ["cookies", "body", "url"]


class Session(BaseSession):

    DEFAULT_ARGS = {"desired_capabilities": {'javascriptEnabled': True,
                                             'takeScreenshot': False}}
    DEFAULT_CLASS = selenium.webdriver.Remote

    def __init__(self, driver_class=None, driver_args=None, driver=None):
        super(Session, self).__init__(driver_class, driver_args, driver)
        self._current_form = None
        self._response = None

    def get_link_href(self, selector):
        from selenium.common.exceptions import NoSuchElementException
        try:
            element = self.driver.find_element_by_xpath(selector)
        except NoSuchElementException as err:
            raise ValueError("Can't get link href", selector, err)
        else:
            return element.get_attribute("href")

    def set_input_by_xpath(self, selector, value):
        from selenium.common.exceptions import NoSuchElementException
        try:
            input = self.driver.find_element_by_xpath(selector)
        except NoSuchElementException:
            raise ValueError("Can't set value for {0}".format(selector))
        try:
            input.send_keys(value)
        except Exception as err:
            raise ValueError(
                "Can't set value for {0} in case of {1}".format(selector,
                                                                str(err)))

    def go(self, url):
        self.driver.get(url)

    def choose_form(self, number=None, id=None, name=None, xpath=None):
        form_element = None
        if number:
            raise NotImplementedError
        if id:
            form_element = self.driver.find_element_by_id(id)
        if name:
            form_element = self.driver.find_element_by_name(name)
        if xpath:
            form_element = self.driver.find_element_by_xpath(xpath)
        self._current_form = form_element

    def submit(self, *args, **kwargs):
        if self._current_form is None:
            self._current_form = self.driver.find_element_by_tag_name("form")
        self._current_form.submit(*args, **kwargs)
        self.driver.save_screenshot("/tmp/last.png")

    def xpath(self, selector):
        try:
            return self.driver.find_element_by_xpath(selector)
        except Exception:
            return []

    def wait(self, time):
        self.logger.debug("Will sleep %s seconds", time)
        sleep(time)

    @property
    def tree(self):
        from lxml.etree import ElementTree as ET
        tree = ET()
        return tree.parse(self.response.body)

    @property
    def response(self):
        if self._response is None:
            self._response = FakeResponse()
        raw_cookies = self.driver.get_cookies()
        self._response.cookies = dict([(cookie["name"], cookie["value"])
                                       for cookie in raw_cookies])
        self._response.body = self.driver.page_source
        self._response.url = self.driver.current_url
        return self._response

    @property
    def form(self):
        raise NotImplementedError

    def click(self, selector):
        try:
            element = self.driver.find_element_by_xpath(selector)
            element.click()
        except Exception as err:
            raise ValueError("Can't click", selector, err)
