from __future__ import absolute_import

__author__ = 'mihail'

import grab

from time import sleep

from contaminant.core.abstract.session import BaseSession
from lxml.etree import XPathError


class Session(BaseSession):

    DEFAULT_ARGS = {}
    DEFAULT_CLASS = grab.Grab

    def get_link_href(self, xpath):
        """
        Select href-attribute of first xpath link

        :param xpath xpath-string:
        :return: string
        :raise: ValueError
        """
        try:
            return dict(self.driver.tree.xpath(
                xpath
            )[0].items())["href"]
        except (IndexError, KeyError, XPathError) as err:
            raise ValueError("Can't get link href", xpath, err)

    def set_input_by_xpath(self, xpath, value):
        try:
            self.driver.set_input_by_xpath(
                xpath,
                value
            )
        except IndexError:
            raise ValueError("Can't set value for {0}".format(xpath))

    def go(self, url):
        return self.driver.go(url)

    def choose_form(self, number=None, id=None, name=None, xpath=None):
        return self.driver.choose_form(number, id, name, xpath)

    def submit(self, *args, **kwargs):
        return self.driver.submit(*args, **kwargs)

    def xpath(self, selector):
        return self.driver.xpath(selector)

    def get_cookies(self):
        return self.driver.response.cookies

    def set_cookies(self, cookies):
        self.driver.setup(cookies=cookies)

    @property
    def tree(self):
        return self.driver.tree

    @property
    def response(self):
        return self.driver.response

    @property
    def form(self):
        return self.driver.form

    def wait(self, time):
        sleep(time)
