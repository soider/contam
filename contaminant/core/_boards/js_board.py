__author__ = 'soider'

from contaminant.core.abstract.board import Board as BaseBoard
from contaminant.core._sessions.phantom import Session as JsSession
from contaminant.core.errors import PostCreatingError, ThreadCreatingError


class Board(BaseBoard):

    def __init__(self, config, session, logger):
        if not isinstance(session, JsSession):
            raise ValueError("Invalid session type for js_board.Board")
        super(Board, self).__init__(config, session, logger)

    def create_thread(self, forum, subject, text):
        self.session.go(
            self.config["root_url"]
        )
        forum_link_xpath = self.config["forum_link_xpath_format"].format(
            forum)
        try:
            self.logger.debug("Try to found link by %s for %s",
                              forum,
                              forum_link_xpath)
            forum_url = self.get_link_href(
                forum_link_xpath
            )
        except ValueError:
            raise ThreadCreatingError(
                "Can't find link to forum {0}".format(forum),
                forum_link_xpath
            )
        self.logger.debug("Find %s", forum_url)
        self.session.go(forum_url)
        try:
            new_thread_link_xpath =\
                self.config["new_thread_link_xpath"]
            self.session.click(
                new_thread_link_xpath
            )
        except ValueError as err:
            raise ThreadCreatingError(
                "Can't find and click on new thread button",
                self.config["new_thread_link_xpath"],
                err
            )
        if self.config.get("new_thread_js_before"):
            self.session.driver.execute_script(
                self.config.get("new_thread_js_before")
            )
        self.session.driver.save_screenshot("/tmp/screen1.png")
        self.session.choose_form(
            **self.config["thread_form"]
        )
        self.session.set_input_by_xpath(
            self.config["new_thread_subject_input_xpath"],
            subject)
        self.session.set_input_by_xpath(
            self.config["new_thread_text_input_xpath"],
            text
        )
        self.session.driver.save_screenshot("/tmp/huita.png")
        if self.config.get("new_thread_js_after"):
            self.session.driver.execute_script(
                self.config.get("new_thread_js_after")
            )

        self.session.submit()
        if self.config.get("new_thread_wait_on_page_after"):
            self.session.wait(
                self.config.get("new_thread_wait_on_page_after")
            )
        if self.session.xpath(
            self.config['new_thread_error_message_xpath']
        ) \
            and \
                self.session.xpath(
                    self.config['new_thread_error_message_xpath']
                ).text.strip():
                self.session.driver.save_screenshot("/tmp/error_thread.png")
                raise ThreadCreatingError(
                    self.session.xpath(
                        self.config['new_thread_error_message_xpath']
                    ).text.strip()
                )
        self.session.driver.save_screenshot("/tmp/huita.png")
        self.logger.debug("Created thread %s", self.session.response.url)
        return self.session.response.url

    def create_post(self, thread, text):
        self.logger.debug("begin creating post in thread %s", thread)
        self.session.go(thread)
        self.session.choose_form(**self.config["post_form"])

        if self.config.get("new_post_js_before"):
            self.session.driver.execute_script(
                self.config.get("new_post_js_before")
            )

        self.session.set_input_by_xpath(
            self.config["new_post_text_input_xpath"],
            text
        )

        if self.config.get("new_post_js_after"):
            self.session.driver.execute_script(
                self.config.get("new_post_js_after")
            )

        self.session.submit()
        if self.config.get("new_post_wait_on_page_after"):
            self.session.wait(
                self.config["new_post_wait_on_page_after"]
            )
        if self.session.xpath(
            self.config['new_post_error_message_xpath']
        ) and \
                self.session.xpath(
                    self.config['new_post_error_message_xpath']
                ).text.strip() and \
                self.session.xpath(
                    self.config['new_post_error_message_xpath']
                ).is_displayed():
            self.session.driver.save_screenshot("/tmp/error_post.png")
            raise PostCreatingError(
                self.session.xpath(
                    self.config['new_post_error_message_xpath']
                ).text.strip()
            )
