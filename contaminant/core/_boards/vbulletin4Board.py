__author__ = 'soider'

from hashlib import md5

from contaminant.core._boards.nojs_board import Board


class Vbulletin4Board(Board):

    TEMPLATE_FIELDS = ['new_post_wait_on_page',
                       'post_form',
                       'new_thread_subject_input_xpath',
                       'new_post_error_message_xpath',
                       'login_form',
                       'new_thread_wait_on_page_after',
                       'login_input_xpath',
                       'new_thread_link_xpath',
                       'new_post_text_input_xpath',
                       'new_post_wait_on_page_after',
                       'new_post_link_xpath',
                       'thread_form',
                       'new_thread_submit_args',
                       'password_input_xpath',
                       'vbulletin_unauthorized_element',
                       'new_thread_error_message_xpath',
                       'forum_link_xpath_format',
                       'new_thread_text_input_xpath',
                       'new_thread_wait_on_page',
                       'new_post_submit_args']

    def authorize(self, login, password):
        return super(Vbulletin4Board,
                     self).authorize(login,
                                     md5(password).hexdigest()
                                     )

    def is_authorized(self):
        self.session.go(self.config["root_url"])
        if self.session.tree.xpath(
                self.config["vbulletin_unauthorized_element"]
        ):
            return False
        return True


if __name__ == "__main__":
    import logging
    logger = logging.getLogger('grab')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)
    my_logger = logging.getLogger("my")
    my_logger.addHandler(logging.StreamHandler())
    my_logger.setLevel(logging.DEBUG)

    config = {
        "root_url": "http://192.168.1.71/vbul/forum.php",
        "login_url": "http://192.168.1.71/vbul/forum.php",
        "login_input_xpath": "//input[@name='vb_login_username']",
        "password_input_xpath": "//*[@name='vb_login_md5password']",
        "vbulletin_unauthorized_element": "//input[@class='loginbutton']",
        "login_form": {
            "id": "navbar_loginform"
        },
        "post_form": {
            "name": "vbform"
        },
        "thread_form": {
            "name": "vbform"
        },
        "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
        "new_thread_link_xpath": "//a[text()\
[contains(., 'Post New Thread')]]",
        "new_thread_text_input_xpath": "//textarea[@name='message']",
        "new_thread_subject_input_xpath": "//input[@name='subject']",
        "new_thread_submit_args": {
            "submit_name": "sbutton"
        },
        "new_thread_wait_on_page": 10,
        "new_thread_wait_on_page_after": 10,
        "new_thread_error_message_xpath":
"//div[@class='blockbody errorblock'] | //*[@class='standard_error']",

        "new_post_link_xpath": "//a[text()[contains(., 'Reply to Thread')]]",
        "new_post_wait_on_page": 10,
        "new_post_wait_on_page_after": 0,
        "new_post_text_input_xpath": "//textarea[@name='message']",
        "new_post_submit_args": {
            "submit_name": "sbutton"
        },
        "new_post_error_message_xpath":
"//div[@class='blockbody errorblock'] | //*[@class='standard_error']",
    }

    from contaminant.core.sessions import GrabSession

    session = GrabSession()

    board = Vbulletin4Board(config, session, logger)

    board.authorize("soider", "spb43afk")
    url = board.create_thread(
        "Main Forum",
        "New test HOOHOHOHOO",
        "Test content for fucking test"
    )
    board.create_post(url,
                      "Some message in new topeg!!")
    open("/tmp/after.html", "w").write(board.session.response.body)
