# *-* coding: utf-8 *-*
__author__ = 'mihail'

from contaminant.core.errors import \
    InvalidAuthorization
from contaminant.core._boards.js_board import Board

from pprint import pprint
p = pprint


class VbulletinBoard(Board):

    TEMPLATE_FIELDS = ['new_thread_error_message_xpath',
                       'login_input_xpath',
                       'new_post_text_input_xpath',
                       'new_post_js_after_',
                       'new_thread_link_xpath',
                       'forum_link_xpath_format',
                       'new_thread_subject_input_xpath',
                       'post_form',
                       'new_thread_wait_on_page_after',
                       'new_thread_js_before',
                       'password_input_xpath',
                       'vbulletin_unauthorized_element',
                       'new_thread_js_after_',
                       'new_thread_text_input_xpath',
                       'new_post_error_message_xpath',
                       'new_post_js_before',
                       'login_form',
                       'new_post_wait_on_page_after',
                       'thread_form']

    def authorize(self, login, password):
        from selenium.common.exceptions import WebDriverException
        try:
            super(VbulletinBoard, self).authorize(login, password)
        except Exception:
            self.session.wait(10)
            try:
                if not self.is_authorized():
                    raise InvalidAuthorization("Can't authorize",
                                               self.session.response)
            except WebDriverException:
                raise InvalidAuthorization("Can't authorize",
                                           self.session.response)

    def is_authorized(self):
        try:
            if self.session.xpath(
                self.config["vbulletin_unauthorized_element"]
            ):
                raise InvalidAuthorization()
        except InvalidAuthorization:
            return False
        return True

if __name__ == "__main__":
    import logging
    from contaminant.core.sessions import PhantomSession
    from selenium import webdriver
    logger = logging.getLogger("vbulletin")
    logger.setLevel(logging.DEBUG)
    logger.addHandler(logging.StreamHandler())

    root_url = "http://084ad0f5-8463-4374-9b1f-142328510563.\
    sitebuilder.vbulletin.com/"
    login_url = "http://084ad0f5-8463-4374-9b1f-142328510563.\
    sitebuilder.vbulletin.com/auth/login-form"

    config = {
        "root_url": root_url,
        "login_url": login_url,
        "login_input_xpath": '//input[@id="idLoginUserName"]',
        "password_input_xpath": '//input[@id="idLoginPassword"]',
        "vbulletin_unauthorized_element": '//*[@id="lnkLoginSignupMenu"]',
        "login_form": {
            "id": "idLoginUserName"
        },
        "post_form": {
            "id": "newTextForm"
        },
        "thread_form": {
            "id": "newTextForm"
        },

        "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
        "new_thread_link_xpath": "//*[text()[contains(., '+ New Topic')]]",
        "new_thread_js_before": """
        document.getElementById('ckeditor-bare').style.display='block';
        document.getElementById('ckeditor-bare').style.visibility = 'visible'
                """,
        "new_thread_js_after_": """
document.getElementById('ckeditor-bare').style.display='none';
document.getElementById('ckeditor-bare').style.visibility = 'hidden'""",
        "new_thread_text_input_xpath": "//textarea[@id='ckeditor-bare']",
        "new_thread_subject_input_xpath": "//input[@name='title']",
        "new_thread_error_message_xpath": "//div[@id='alert-dialog']",
        "new_post_js_before": """
document.getElementById('ckeditor-bare').style.display='block';
document.getElementById('ckeditor-bare').style.visibility = 'visible'""",
        "new_post_js_after_": """
document.getElementById('ckeditor-bare').style.display='none';
document.getElementById('ckeditor-bare').style.visibility = 'hidden'""",
        "new_post_text_input_xpath": "//textarea[@id='ckeditor-bare']",
        "new_post_error_message_xpath": "//div[@id='alert-dialog']",
        "new_thread_wait_on_page_after": 10,
        "new_post_wait_on_page_after": 10
    }

    session = PhantomSession(
        driver=webdriver.PhantomJS()
    )
    board = VbulletinBoard(config, session, logger)
    try:
        board.authorize("sahnov-m@yandex.ru", "Mcc6yahIeRo7n7At")
        url = board.create_thread(
            "Main Forum",
            "This is hello world",
            ""
        )
        logger.debug("Createdf %s", url)
        import time
        time.sleep(2)
        board.create_post(url,
                          "Hello world ept")
    finally:
        board.session.driver.quit()
