__author__ = 'soider'

from re import match, search

from contaminant.core._boards.nojs_board import Board


class PhpBoard(Board):

    TEMPLATE_FIELDS = [
        "login_input_xpath",
        "password_input_xpath",
        "login_form",
        "post_form",
        "thread_form",
        "forum_link_xpath_format",
        "new_thread_link_xpath",
        "new_thread_text_input_xpath",
        "new_thread_subject_input_xpath",
        "new_thread_submit_args",
        "new_thread_wait_on_page",
        "new_thread_wait_on_page_after",
        "new_thread_error_message_xpath",
        "new_post_link_xpath",
        "new_post_wait_on_page",
        "new_post_wait_on_page_after",
        "new_post_text_input_xpath",
        "new_post_submit_args",
        "new_post_error_message_xpath"
    ]

    def is_authorized(self):
        cookies = self.session.response.cookies.items()
        for name, value in cookies:
            if match("phpbb3_(.+)_u", name):
                if value != '1':
                    return True
        return False

    def get_new_thread_url(self):
        selector = self.config.get("redirect_selector")
        if selector is None:
            selector = "//meta[@http-equiv='refresh']"
        val = dict(self.session.xpath(selector).items())["content"]
        return search(".+(http://.+)", val).groups()[0]


if __name__ == "__main__":
    import logging
    logger = logging.getLogger('grab')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)
    my_logger = logging.getLogger("my")
    my_logger.addHandler(logging.StreamHandler())
    my_logger.setLevel(logging.DEBUG)

    config = {
        "root_url": "http://192.168.1.71/forum/",
        "login_url": "http://192.168.1.71/forum/ucp.php?mode=login",
        "login_input_xpath": "//input[@name='username']",
        "password_input_xpath": "//input[@name='password']",
        "login_form": {
            "id": "login"
        },
        "post_form": {
            "id": "postform"
        },
        "thread_form": {
            "id": "postform"
        },
        "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
        "new_thread_link_xpath": "//a[text()\
[contains(., 'Post a new topic')]]",
        "new_thread_text_input_xpath": "//textarea[@name='message']",
        "new_thread_subject_input_xpath": "//input[@name='subject']",
        "new_thread_submit_args": {
            "submit_name": "post"
        },
        "new_thread_wait_on_page": 10,
        "new_thread_wait_on_page_after": 10,
        "new_thread_error_message_xpath": "//p[@class='error']",

        "new_post_link_xpath": "//a[text()[contains(., 'Post a reply')]]",
        "new_post_wait_on_page": 10,
        "new_post_wait_on_page_after": 0,
        "new_post_text_input_xpath": "//textarea[@name='message']",
        "new_post_submit_args": {
            "submit_name": "post"
        },
        "new_post_error_message_xpath": "//p[@class='error']",
    }

    import grab
    from contaminant.core.sessions import GrabSession, PhantomSession
    from selenium import webdriver

    session = GrabSession(
        #    grab.Grab(debug_post=True)
    )

    board = PhpBoard(config, session, logger)
    board.authorize("soider", "spb43afk")
    url = board.create_thread(
        "Your first forum",
        "New test #777777666",
        "Test content for fucking test"
    )

    # url = "http://192.168.1.71/forum/viewtopic.php?f=2&t=31"
    board.create_post(url,
                      "Some message in new topeg!!")
