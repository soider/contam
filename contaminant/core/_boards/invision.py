__author__ = 'mihail'

from contaminant.core._boards.nojs_board import Board


class InvisionDefaultBoard(Board):

    """Board subclass specialized on IPB forums accessed without JS """

    TEMPLATE_FIELDS = [
        "login_input_xpath",
        "password_input_xpath",
        "login_form",
        "post_form",
        "thread_form",
        "forum_link_xpath_format",
        "new_thread_wait_on_page_after",
        "new_thread_link_xpath",
        "new_thread_text_input_xpath",
        "new_thread_subject_input_xpath",
        "new_thread_error_message_xpath",
        "new_post_link_xpath",
        "new_post_text_input_xpath",
        "new_post_error_message_xpath"
    ]

    def is_authorized(self):
        """Check is active user session

        :return Boolean
        """
        if "session_id" in self.session.response.cookies and \
           "pass_hash" in self.session.response.cookies:
                return True
        return False


if __name__ == "__main__":
    import logging
    logger = logging.getLogger('grab')
    logger.addHandler(logging.StreamHandler())
    logger.setLevel(logging.DEBUG)
    my_logger = logging.getLogger("my")
    my_logger.addHandler(logging.StreamHandler())
    my_logger.setLevel(logging.DEBUG)
    url = "http://a39554.demo.invisionpower.com/\
index.php?app=core&module=global&section=login"
    config = {
        "root_url": "http://a39554.demo.invisionpower.com",
        "login_url": url,
        "login_input_xpath": "//input[@name='ips_username']",
        "password_input_xpath": "//input[@name='ips_password']",
        "login_form": {
            "id": "login"
        },
        "post_form": {
            "id": "postingform"
        },
        "thread_form": {
            "id": "postingform"
        },
        "forum_link_xpath_format": "//a[@title='{0}']",
        "new_thread_wait_on_page_after": 3,
        "new_thread_link_xpath": "//a[text()[contains(., 'Start New Topic')]]",
        "new_thread_text_input_xpath": "//textarea[@name='Post']",
        "new_thread_subject_input_xpath": "//input[@name='TopicTitle']",
        "new_thread_error_message_xpath": "//p[@class='message error']",
        "new_post_link_xpath": "//a[text()\
[contains(., 'Reply to this topic')]]",
        "new_post_text_input_xpath": "//textarea[@name='Post']",
        "new_post_error_message_xpath": "//p[@class='message error']",
    }

    import grab
    from contaminant.core.sessions import GrabSession

    session = GrabSession(
        grab.Grab(debug_post=True)
    )

    board = InvisionDefaultBoard(config, session, logger)
    board.authorize("admin", "bKCnoVsW")
    url = board.create_thread(
        "A Test Forum",
        "Very new forum",
        "Some text123"
    )

    board.create_post(url,
                      "Some message")
