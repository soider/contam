__author__ = 'soider'

import time

from contaminant.core.abstract.board import Board as BaseBoard
from contaminant.core._sessions.grab import Session as NoJsSession
from contaminant.core.errors import PostCreatingError, ThreadCreatingError


class Board(BaseBoard):

    """Board subclass specialized on forums accessed without JS

        Default scenario:
            1) Open index page
            2) Open login page
            3) Fill inputs
            4) Submit
            5) Open index page
            6) Open forum page
            7) Open create thread
            8) Fill
            9) Submit
            10) Open thread page
            11) Fill
            12) Submit
    """

    def __init__(self, config, session, logger):
        logger.debug("Need %s", str(NoJsSession))
        logger.debug("Got %s", str(session))
        logger.debug("Type of got %s", str(type(session)))
        if not isinstance(session, NoJsSession):
            raise ValueError("Invalid session type for nojs_board.Board")
        super(Board, self).__init__(config, session, logger)

    def create_post(self, thread, text):
        """

        :param thread:  thread url
        :param text: message text
        :raise: ValueError
        """
        try:
            self.logger.debug("Begin creating post")
            self.logger.debug("%s %s", thread, text)
            self.session.go(thread)
            try:
                form_url = self.get_link_href(
                    self.config["new_post_link_xpath"]
                )
            except ValueError:
                raise PostCreatingError(
                    "Can't find new post link",
                    self.config["new_post_link_xpath"],
                    self.session.response
                )
            self.logger.debug("Go to page with post form")
            self.session.go(form_url)
            try:
                self.set_input_by_xpath(
                    self.config["new_post_text_input_xpath"],
                    text
                )
            except ValueError as err:
                raise PostCreatingError(
                    str(err),
                    self.config["new_post_text_input_xpath"],
                    self.session.response
                )
            self.session.choose_form(
                **self.config["post_form"]
            )
            if self.config.get("new_post_wait_on_page"):
                self.session.wait(self.config["new_post_wait_on_page"])
            self.logger.debug("Submitting post form")
            if self.config.get("new_post_submit_args"):
                self.session.submit(
                    **self.config["new_post_submit_args"]
                )
            else:
                self.session.submit()
            if self.config.get("new_post_wain_on_page_after"):
                self.session.wait(self.config["new_post_wain_on_page_after"])
            if self.session.tree.xpath(
                    self.config['new_post_error_message_xpath']
            ):
                raise PostCreatingError(
                    self.session.xpath(
                        self.config['new_post_error_message_xpath']
                    ).text_content().strip()
                )
        except Exception as err:
            raise PostCreatingError(
                "Can't create post: error {0}".format(str(err)),
                err
            )

    def create_thread(self, forum, subject, text):
        """

        Create thread in specified forum

        :param forum: string xpath-selector for link tag
                          forum section in which should topic create

        :param subject: string topic name
        :param text: string topic text
        :return url
        """
        try:
            self.logger.debug("Create thread begin")
            self.session.go(
                self.config['root_url']
            )
            forum_xpath = self.config["forum_link_xpath_format"].format(
                forum
            )
            try:
                forum_url = self.get_link_href(forum_xpath)
            except ValueError as err:
                raise ThreadCreatingError(
                    "Can't find link to forum page",
                    forum_xpath,
                    err
                )
            self.logger.debug("Go to forum {0}".format(str(forum_url)))
            self.session.go(forum_url)
            try:
                url = self.get_link_href(self.config["new_thread_link_xpath"])
            except ValueError:
                raise ThreadCreatingError(
                    "Can't find link to new thread page",
                    self.config["new_thread_link_xpath"],
                    self.session.response
                )
            self.logger.debug("Go to page with new thread form {0}".format(url))
            self.session.go(url)
            try:

                self.set_input_by_xpath(
                    self.config["new_thread_subject_input_xpath"],
                    subject
                )
                self.set_input_by_xpath(
                    self.config["new_thread_text_input_xpath"],
                    text
                )
            except ValueError as err:
                raise ThreadCreatingError(
                    str(err),
                    self.config["new_thread_subject_input_xpath"],
                    self.config["new_thread_text_input_xpath"]
                )
            self.session.choose_form(
                **self.config["thread_form"]
            )
            self.logger.debug("Submit new thread form")
            if self.config.get("new_thread_wait_on_page"):
                self.session.wait(
                    self.config.get("new_thread_wait_on_page")
                )
            if self.config.get("new_thread_submit_args"):
                self.session.submit(
                    **self.config["new_thread_submit_args"]
                )
            else:
                self.session.submit()
            if self.config.get("new_thread_wait_on_page_after"):
                self.session.wait(
                    self.config.get("new_thread_wait_on_page_after")
                )
            if self.session.tree.xpath(
                self.config['new_thread_error_message_xpath']
            ):
                raise ThreadCreatingError(
                    self.session.xpath(
                        self.config['new_thread_error_message_xpath']
                    ).text_content().strip()
                )
            try:
                url = self.get_new_thread_url()
                self.logger.debug("Created thread %s", url)
                return url
            except Exception as err:
                raise ThreadCreatingError(
                    "Can't get new thread url",
                    str(err)
                )
        except Exception as err:
                raise ThreadCreatingError(
                    "Can't create thread: error {0}".format(str(err)),
                    err
                )