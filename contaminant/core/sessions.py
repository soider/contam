__author__ = 'mihail'

from contaminant.core._sessions.grab import Session as GrabSession
from contaminant.core._sessions.phantom import Session as PhantomSession


sessions = {
    "VbulletinBoard": PhantomSession
}


def get_board_session(board_class):
    return sessions.get(board_class, GrabSession)
