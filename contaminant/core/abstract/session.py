__author__ = 'mihail'


class BaseSession(object):

    def __init__(self, driver_class=None, driver_args=None, driver=None):
        if driver is not None:
            self.driver = driver
        else:
            if driver_class is None:
                driver_class = self.DEFAULT_CLASS
            if driver_args is None:
                driver_args = self.DEFAULT_ARGS
            self.driver = driver_class(**driver_args)

    def get_link_href(self, xpath):
        raise NotImplementedError

    def set_input_by_xpath(self, xpath, value):
        raise NotImplementedError

    def go(self, url):
        raise NotImplementedError

    def choose_form(self, number=None, id=None, name=None, xpath=None):
        raise NotImplementedError

    def submit(self, *args, **kwargs):
        raise NotImplementedError

    def xpath(self, selector):
        raise NotImplementedError

    def get_cookies(self):
        raise NotImplementedError

    def set_cookies(self):
        raise NotImplementedError

    @property
    def tree(self):
        raise NotImplementedError

    @property
    def response(self):
        raise NotImplementedError

    @property
    def form(self):
        raise NotImplementedError
