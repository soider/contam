__author__ = 'mihail'

import logging

from contaminant.core.errors import InvalidAuthorization, InvalidConfig


class Board(object):

    def __init__(self, config, session, logger=None):
        """Init

        :param config - configuration object
        :param session - session request object
        :param logger - logging.Logger instance
        """
        self.config = config
        self.session = session
        if logger is None:
            logger = self.get_logger()
        self.logger = logger

    def authorize(self, login, password):
        """
        Authorize on board

        :param login: username
        :param password: password
        """
        self.session.go(
            self.config["root_url"]
        )
        if not self.is_authorized():
            self.logger.debug("Open index")
            self.session.go(
                self.config["login_url"]
            )
            try:
                self.set_input_by_xpath(
                    self.config["login_input_xpath"],
                    login
                )
                self.set_input_by_xpath(
                    self.config["password_input_xpath"],
                    password
                )
            except ValueError as err:
                raise InvalidAuthorization("Can't find inputs",
                                           self.config["login_input_xpath"],
                                           self.config["password_input_xpath"]
                                           )
            self.session.choose_form(
                **self.config["login_form"]
            )
            self.session.submit()

            if not self.is_authorized():
                raise InvalidAuthorization(
                    "Invalid authorization, see response",
                    self.session.response)
            self.logger.debug("Successfully authorized")
        else:
            self.logger.debug("Already authorized, skip")

    def set_input_by_xpath(self, xpath, value):
        """
        Proxy to self.session.set_input_by_xpath

        :param xpath: xpath-string
        :param value: value
        :raise: ValueError
        """
        self.session.set_input_by_xpath(
            xpath,
            value
        )

    def get_link_href(self, xpath):
        """
        Select href-attribute of first xpath link

        :param xpath xpath-string:
        :return: string
        :raise: ValueError
        """

        return self.session.get_link_href(xpath)

    def is_authorized(self):
        """

        Check is authorized

        :raise InvalidAuthorization
        """
        raise NotImplementedError

    def create_thread(self, forum, subject, text):
        """

        :param subject: topic subject
        :param text: topic text
        """
        raise NotImplementedError

    def create_post(self, thread, text):
        """

        :param thread: topic id
        :param text: post text
        """
        raise NotImplementedError

    def get_new_thread_url(self):
        """
        :return string last created thread url
        """
        return self.session.response.url

    @classmethod
    def validate_template_config(cls, config):
        """
        :param config dict object with board config
        """
        logger = cls.get_logger()
        logger.debug("Begin validating config")
        logger.debug(config)
        for field in cls.TEMPLATE_FIELDS:
            if field not in config:
                raise InvalidConfig("No field {0} in config".format(field))

    @classmethod
    def get_logger(cls):
        return logging.getLogger(
            "contaminant.core.boards.{0}".format(
            cls.__name__)
        )
