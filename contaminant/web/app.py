from __future__ import absolute_import

__author__ = 'soider'


from flask import Flask

from contaminant.tools.decorators import inject_logger
from contaminant import conf


from contaminant.web.tools import ContaminantApi


@inject_logger
def build_application(logger):
    logger.debug("Building app")
    app = make_application()
    api = make_api(app)
    from contaminant.web.urls import urls
    resources = get_urls(urls)
    add_urls(resources, api)
    return app


def make_application():
    app = Flask(__name__)
    app.config["DEBUG"] = conf.DEBUG
    app.config["SECRET_KEY"] = conf.SECRET
    #app.config["LOGGER_NAME"] = "contaminant.web.application"
    return app


def make_api(app):
    return ContaminantApi(app)


def get_urls(urls):
    result = []
    for module_name, klass_name, url in urls:
        module_path = "contaminant.web.resources.{0}".format(
            module_name
        )
        klass = getattr(
            getattr(__import__(module_path).web.resources, module_name),
            klass_name)
        result.append((klass, url))
    return result


@inject_logger
def add_urls(urls, api, logger):
    for klass, url in urls:
        logger.debug("append %s %s" % (klass, url))
        api.add_resource(klass, url)


if __name__ == '__main__':

    app = build_application()
    import contaminant.web.current
    contaminant.web.current.app = app
    app.run()
