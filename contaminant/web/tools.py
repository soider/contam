__author__ = 'soider'

import difflib
import re
import json
from flask import request,  got_request_exception
from flask.ext import restful
from flask.ext.restful.utils import unauthorized, error_data


def minimum_length(length, _type=unicode):
    """Return type checker, that checks sequence
    length is great or equal that length

    :param length integer minimal length
    :return function
    """
    def inner(val):
        """
        :param val any sequence
        :return sequence
        :raise ValueError
        """
        try:
            val = _type(val)
            if len(val) < length:
                raise ValueError("Must be longer that {0}".format(length))
            return val
        except ValueError as err:
            raise err
        except Exception as err:
            raise ValueError("{0} is invalid {1}: {2}".format(
                val, _type.__name__, str(err)))

    return inner


def json_type(val):
    """Simple json type converter"""
    try:
        return json.loads(val)
    except ValueError:
        raise ValueError("Invalid json format")


class ContaminantApi(restful.Api):

    """
    restful.Api with custom handle_error
    """
    def handle_error(self, e):
        """Error handler for the API transforms a raised exception into a Flask
        response, with the appropriate HTTP status code and body.

        :param e: the raised Exception object
        :type e: Exception

        """
        got_request_exception.send(self, exception=e)
        code = getattr(e, 'code', 500)
        data = getattr(e, 'data', error_data(code))
        data["status"] = code

        if code >= 500:
            self.app.logger.exception("Internal Error")

        if code == 404:
            rules = dict([(re.sub('(<.*>)', '', rule.rule), rule.rule)
                          for rule in self.app.url_map.iter_rules()])
            close_matches = difflib.get_close_matches(
                request.path, rules.keys())
            if close_matches:
                # If we already have a message, add punctuation and continue
                # it.
                if "message" in data:
                    data["message"] += ". "
                else:
                    data["message"] = ""
                print dir(self)
                if self.app.config["DEBUG"]:
                    data['message'] += 'You have requested this URI [' \
                        + request.path + \
                        '] but did you mean ' + \
                        ' or '.join(
                                       (rules[match]
                                        for match in close_matches)) + ' ?'

        data = {
            "error": data
        }
        resp = self.make_response(data, code)

        if code == 401:
            resp = unauthorized(resp,
                                self.app.config.get("HTTP_BASIC_AUTH_REALM",
                                                    "flask-restful"))
        return resp
