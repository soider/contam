__author__ = 'soider'

from contaminant.web.resources import DefaultPrivateResource
from contaminant.web.tools import minimum_length

from flask_restful.types import url
from contaminant.data.models.task import Task
from contaminant.data.models.profile import Profile

from contaminant.workflow.tasks import create_task

from flask_restful import abort


class TaskCreator(DefaultPrivateResource):

    POST_PARAMETERS = [

        {"name": "action",
         "type": str,
         "help": "action is required and \
must be one of: create_post, create_thread",
         "required": True,
         "choices": ("create_post", "create_thread")
         },
        {
            "name": "profile",
            "type": str,
            "help": "profile is required",
            "required": True
        },
        {
            "name": "text",
            "type": minimum_length(15),
            "help": "text is required",
            "required": True
        },
        {
            "name": "subject",
            "type": minimum_length(15),
            "help": "thread subject required \
with action=create_thread and must be longer that 15 symbols",
        },
        {
            "name": "thread",
            "type": url,
            "help": "thread url is required with action=create_post"
        },
        {
            "name": "forum",
            "type": unicode,
            "help": "forum is required with action=create_thread"

        }
    ]

    POST_PARAMETERS_META = [
        {
            "name": "action",
            "constraints": [
                # create_thread constraints
                (lambda action, args:
                 (action == "create_thread"
                  and args.get("subject", None) is not None)
                    or (action == "create_post"),
                    "Subject must be set when action is create_thread"),

                (lambda action, args:
                 (action == "create_thread"
                  and args.get("forum", None) is not None)
                 or (action == "create_post"),
                 "forum must be set when action is create_thread"),

                # create_post constraints
                (lambda action, args:
                 (action == "create_post"
                  and args.get("thread", None) is not None) or
                 (action == "create_thread"),
                 "thread must be correct url when action is create_post"),

            ]
        }
    ]

    def get(self):
        tasks = Task.objects.find_by_user(
            self.auth["user"].id
        )
        return [task.get_description()
                for task in tasks]

    def post(self):
        self.logger.debug("Begin creating task")

        action = self.post_args.pop("action")
        kwargs = dict(
            (
                (key, value) for (key, value) in self.post_args.items()
                if value is not None
            )
        )
        kwargs["profile"] = Profile.find_by_id(kwargs["profile"]).get_profile()
        id = create_task(
            action,
            **kwargs
        )
        self.logger.debug("Action %s", action)
        self.logger.debug("Args: %s", kwargs)
        self.logger.debug("Created celery task <id> %s", str(id))
        task_log = Task()
        task_log.set_user_by_id(
            self.auth["user"].id
        )
        task_log.celery_id = str(id)
        task_log.save()
        self.logger.debug("Save to mongo log %s", task_log.id)
        return {"id": str(task_log.id)}


class TaskModifier(DefaultPrivateResource):

    def get(self, id):
        try:
            return Task.find_by_user_and_id(
                self.auth["user"].id,
                id
            ).get_description()

        except ValueError as er:
            abort(404, message=er)
