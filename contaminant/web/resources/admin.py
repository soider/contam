__author__ = 'soider'

from contaminant.web.resources import AdminResource
from contaminant.web.tools import minimum_length, json_type as json
from contaminant.data.models.template import Template

from contaminant.core.boards import is_valid_board_class_name, get_board_class
from contaminant.core.errors import InvalidConfig

from flask_restful import abort


class TestAdmin(AdminResource):

    def get(self):
        return "ADmin get"


def board_class(val):
    if is_valid_board_class_name(val):
        return val
    else:
        raise ValueError("No such board_class")


class TemplateAdmin(AdminResource):

    POST_PARAMETERS = [
        {
            "name": "name",
            "type": minimum_length(10),
            "required": True
        },
        {
            "name": "board_class",
            "type": board_class,
            "required": True
        },
        {
            "name": "config",
            "required": True,
            "type": json
        }

    ]

    def post(self):
        self.logger.debug("Creating new template")
        try:
            board_klass = get_board_class(
                self.post_args["board_class"]
            )
            board_klass.validate_template_config(
                self.post_args["config"]
            )
        except InvalidConfig as err:
            abort(400,
                  message="Invalid config",
                  details=str(err))
        else:
            tpl = Template()
            (tpl.name, tpl.board_class, tpl.config) =\
                (self.post_args["name"],
                 self.post_args["board_class"],
                 self.post_args["config"])
            tpl.save()
            return {
                "message": "Created new template",
                "id": str(tpl.id)
            }
