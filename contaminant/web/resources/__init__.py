from __future__ import absolute_import

__author__ = 'soider'

import logging
from functools import wraps

from flask import request
from flask.ext import restful
from flask.ext.restful import reqparse
from flask_restful import abort

from contaminant.data.models.token import Token
from contaminant.web.representations import output_json


class DefaultResource(restful.Resource):

    """Base class for open resources"""

    representations = {
        'application/json': output_json
    }

    def __getattribute__(self, name):
        """Auto parse paramaters on request"""
        if name in ("post", "put", "get", "head", "delete"):
            self.add_arguments(
                "{0}_PARAMETERS".format(name.upper()),
                "{0}_parser".format(name),
                "{0}_args".format(name)
            )
        return super(DefaultResource, self).__getattribute__(name)

    def __init__(self, *args, **kwargs):
        """
        Base constructor, set logger, request and parse arguments
        """
        if "logger" in kwargs:
            self.logger = kwargs["logger"]
        else:
            self.logger = logging.getLogger("contaminant.web.resources")
        self.request = request

    def add_arguments(self, field, parser_name, args_name):
        parser = reqparse.RequestParser()
        self.logger.debug("Create parser for %s %s %s",
                          field,
                          parser_name,
                          args_name)
        setattr(self, parser_name, parser)
        if hasattr(self, field):
            self.logger.debug("Have scheme description for %s %s %s",
                              field,
                              parser_name,
                              args_name
                              )
            params = getattr(self, field)
            for arg in params:
                parser.add_argument(**arg)
            args = parser.parse_args()
            # Check meta
            meta_description = getattr(self, "{0}_META".format(field), [])
            for meta_parameter in meta_description:
                if "constraints" in meta_parameter:
                    if not all(
                        [func(args[meta_parameter["name"]], args) for (
                            func, msg) in meta_parameter["constraints"]]
                    ):
                        msgs = [msg for (func, msg)
                                in meta_parameter["constraints"]
                                if not func(
                                    args[meta_parameter["name"]],
                                    args)]
                        abort(400, message=msgs)
            setattr(self, args_name, args)


class BasicMethodDecorator(object):

    """Base class for all method decorators
    store original controller in self.orig_self
    """
    def __init__(self, func):
        if isinstance(func, BasicMethodDecorator):
            self.orig_self = func.orig_self
        else:
            self.orig_self = func.__self__
        self.func = func


class AuthDecorator(BasicMethodDecorator):

    def __call__(self, *args, **kwargs):
        func = self.func
        try:
            token = self.token_validator(self.orig_self.auth["token"])
            login = self.orig_self.auth["login"]
            if not token["user"]["login"] == login:
                raise ValueError("Bad token login")
            self.orig_self.contaminant_auth = dict(
                user=token["user"],
                token=token
            )
            self.orig_self.auth["user"] = token["user"]
        except ValueError:
            restful.abort(401, message="Invalid authorization")
        return func(*args, **kwargs)

    def token_validator(self, token):
        """
        Check that token is valid and return it
        :param token string
        :return Token instance
        """
        if Token.is_valid_token(token):
            return Token.find_by_token(token)[0]
        else:
            raise ValueError("Invalid token")


class AdminOnlyDecorator(BasicMethodDecorator):

    def __call__(self, *args, **kwargs):
        if self.orig_self.auth["user"].role != 1:
            abort(404)
        else:
            return self.func(*args, **kwargs)


class DefaultPrivateResource(DefaultResource):

    """Base class for token protected resources"""

    method_decorators = [AuthDecorator]

    def __init__(self, *args, **kwargs):
        """Always add auth headers to default parser"""
        super(DefaultPrivateResource, self).__init__(*args, **kwargs)
        self.add_auth_arguments()

    def add_auth_arguments(self, parser_name="auth_parser", args_name="auth"):
        """Add X-Contaminant-Login and X-Contaminant-Token
        to arguments headers into default parser"""

        self.logger.debug("add authorization to parser %s", parser_name)
        try:
            parser = getattr(self, parser_name)
        except AttributeError:
            self.logger.debug("Has no such parser, create new")
            parser = reqparse.RequestParser()
        parser.add_argument(
            name="X-Contaminant-Login",
            type=str,
            required=True,
            dest="login",
            location="headers"
        )
        parser.add_argument(
            name="X-Contaminant-Auth",
            type=str,
            required=True,
            dest="token",
            location="headers"
        )
        setattr(self, parser_name, parser)
        setattr(self, args_name, parser.parse_args())


class AdminResource(DefaultPrivateResource):

    """Base class for admin token protected resources"""

    method_decorators = [AdminOnlyDecorator, AuthDecorator]
