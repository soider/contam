__author__ = 'soider'

from contaminant.web.resources import DefaultResource

from contaminant.data.models.user import User

from contaminant.web.tools import minimum_length
from contaminant.workflow.tasks import create_task

from contaminant.data.errors import NoSuchUser

from flask_restful import abort


class Registration(DefaultResource):

    """
    Handles user registrations
    """
    POST_PARAMETERS = [
        {
            "name": "login",
            "type": str,
            "required": True,
            "help": "login is required"
        },
        {
            "name": "password",
            "type": minimum_length(6),
            "required": True,
            "help": "password is required and 6 characters length is minimum"
        }
    ]

    def post(self):
        login = self.post_args["login"]
        try:
            new_user = User.create_new_user_and_save(
                login,
                self.post_args["password"]
            )
        except ValueError:
            return "There is user with such login", 404
        hash = new_user.activate_hash
        create_task(
            "send_activation_mail",
            login=login,
            hash=hash
        )
        return "Mail has been sent"


class Activation(DefaultResource):

    """
    Handle activation
    """
    POST_PARAMETERS = [
        {
            "name": "hash",
            "required": True,
            "help": "hash is required",
            "type": str
        }
    ]

    def post(self):
        hash = self.post_args["hash"]
        user = User.activate_by_hash(hash)
        return "User activated"


class Resend(DefaultResource):

    """
    Resend activation email
    """

    POST_PARAMETERS = [

        {
            "name": "login",
            "required": True,
            "help": "login is required",
            "type": str
        }
    ]

    def post(self):
        login = self.post_args["login"]

        try:
            user = User.objects.by_login(login)
        except NoSuchUser:
            abort(404, message="No such login")
        if user.active:
            return "User activated"
        else:
            create_task(
                "send_activation_mail",
                login=login,
                hash=user.activate_hash
            )
            return "Mail has been sent"
