from contaminant.web.resources import DefaultResource

from contaminant.data.errors import NoSuchUser
from contaminant.data.models.token import Token

from flask_restful import abort

from flask.ext.restful import fields


class Authorize(DefaultResource):

    POST_PARAMETERS = [

        {"name": "login",
         "type": str,
         "help": "User login",
         "required": True},

        {"name": "password",
         "type": str,
         "help": "User password",
         "required": True}

    ]

    def __init__(self, users=None, *args, **kwargs):
        if users is not None:
            self.users = users
        else:
            from contaminant.data.models.user import User
            self.users = User
        super(Authorize, self).__init__(*args, **kwargs)

    def post(self):
        login = self.post_args["login"]
        password = self.post_args["password"]
        try:
            user = self.users.objects.by_login(login)
        except NoSuchUser:
            abort(404, message="No such user")
        if not user.is_valid_password(password):
            abort(401, message="Invalid password")
        if not user.active:
            abort(401, message="Activate before starting")
        Token.delete_user_tokens(user)
        token = Token.create_token(user)
        return token.token
