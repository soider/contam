__author__ = 'soider'

from flask.ext.restful import fields, marshal_with
from flask.ext.restful import types

from flask_restful import abort

from contaminant.web.resources import DefaultPrivateResource
from contaminant.data.models.template import Template as TemplateModel
from contaminant.data.models.profile import Profile as ProfileModel


template_fields = {
    'name':   fields.String,
    'id':    fields.String(attribute="_id")
}


class Template(DefaultPrivateResource):

    @marshal_with(template_fields)
    def get(self):
        templates = [template.to_mongo()
                     for template in TemplateModel.objects.all()]
        return templates

profile_fields = {
    'id': fields.String(attribute="_id"),
    'date_created': fields.DateTime,
    'login': fields.String,
    'password': fields.String,
    'template': fields.String,  # @TODO: Add custom field
    'url': fields.String,
    'login_url': fields.String
}


def set_unrequired(x):
    x = dict(x.items())
    x["required"] = False
    return x


class Profile(DefaultPrivateResource):  # @TODO: base class for crud?

    POST_PARAMETERS = [

        {"name": "login",
         "type": str,
         "required": True,
         "help": "login is required"},

        {"name": "password",
         "type": str,
         "required": True,
         "help": "password is required"},

        {
        "name": "name",
        "type": str,
        "required": True
        "help": "Profile name is required"
        },

        {"name": "template",
         "type": str,
         "required": True,
         "help": "template is required"
         },

        {"name": "url",
         "type": types.url,
         "required": True,
         "help": "url is required"},

        {"name": "login_url",
         "type": types.url,
         "required": True,
         "help": "login_url is required"}
    ]

    PUT_PARAMETERS = map(set_unrequired, POST_PARAMETERS[:])

    FILTER_FIELDS = ('id', 'template')

    @marshal_with(profile_fields)
    def get(self):
        profiles = [profile.to_mongo()
                    for profile in ProfileModel.objects.by_user(
                    self.auth["user"].id
                    )]
        return profiles

    def post(self):
        self.logger.debug("Begin creating new profile from user %s",
                          self.auth["user"].id)
        new_profile = ProfileModel(
            login=self.post_args["login"],
            password=self.post_args["password"],
            url=self.post_args["url"],
            login_url=self.post_args["login_url"],
            name=self.post_args["name"]
        )
        new_profile.set_user_by_id(self.auth["user"].id)
        new_profile.set_template(self.post_args["template"])
        self.logger.debug("Create profile %s",
                          new_profile.to_mongo())
        new_profile.save()
        self.logger.debug("Saved")
        return "Created"

    def put(self, id=None):
        try:
            profile = ProfileModel.find_by_id(id)

        except ValueError as err:
            self.logger.warning("Error while updating profile %s",
                                id)
            self.logger.warning(
                err
            )
            abort(404, message="Invalid id")
        else:
            profile.set_template(self.put_args["template"])
            save_args = dict(
                [(key, value) for (key, value) in self.put_args.items()
                    if value is not None and
                    key not in self.FILTER_FIELDS]
            )
            profile.update_from(**save_args)
            return "Updated"

    def delete(self, id=None):
        try:
            profile = ProfileModel.find_by_id(id)
        except ValueError as err:
            self.logger.warning("Error while deleting profile %s", id)
            self.logger.warning(
                err
            )
            abort(404, message="Invalid id")
        else:
            profile.delete()
            return "Deleted"
