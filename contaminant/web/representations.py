__author__ = 'soider'
import json
from flask import make_response


def output_json(data, code, headers=None):
    if headers is None:
        headers = {}
    answer = {"result": {}}
    answer["result"]["status"] = code
    answer["result"]["answer"] = data
    resp = make_response(json.dumps(answer), code)
    resp.headers.extend(headers)

    return resp
