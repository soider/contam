__author__ = 'soider'

urls = [

    # User registrations
    ("register", "Registration", "/register"),
    ("register", "Activation", "/activate"),
    ("register", "Resend", "/activate/resend"),

    # Get token
    ("authorize", "Authorize", "/login"),

    # Check token
    ("private", "Private", "/private"),

    # Templates
    ("profiles", "Template", "/templates"),

    # Profiles
    ("profiles", "Profile", "/profiles"),
    ("profiles", "Profile", "/profiles/<string:id>"),

    # Tasks
    ("tasks", "TaskCreator", "/tasks"),
    ("tasks", "TaskModifier", "/tasks/<string:id>"),

    # Admin
    ("admin", "TestAdmin", "/admin/test"),
    ("admin", "TemplateAdmin", "/admin/templates")
]
