__author__ = 'soider'
import contaminant.web.current
from contaminant.web.app import build_application

application = build_application()
contaminant.web.current.app = application
