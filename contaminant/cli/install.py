__author__ = 'soider'

import logging

from contaminant.data.install import templates
from contaminant.data.models.template import Template


def main():
    logger = logging.getLogger("contaminant.cli.install")
    for template in templates:
        tpl = Template()
        tpl.config = template["config"]
        tpl.name = template["name"]
        tpl.board_class = template["board_class"]
        try:
            tpl.save()
        except Exception as err:
            logger.warning("Can't save template %s, %s", template[
                           "name"], str(err))

if __name__ == "__main__":
    main()
