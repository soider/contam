# *-* coding: utf-8 *-*
from __future__ import absolute_import

__author__ = 'soider'

import logging
import sys

from celery.task import Task

from contaminant.workflow.tasks import create_thread, create_post

from contaminant.data.fake import profiles


logger = logging.getLogger("contaminant.cli.tools")


def create_thread_task(forum, subject, text, task_profile):
    return create_thread.delay(forum, subject, text, task_profile)


def create_post_task(thread, text, profile):
    return create_post.delay(thread, text, profile)


def check_task(id):
    return Task.AsyncResult(id).state


def get_task_result(id):
    return Task.AsyncResult(id).get()


def main():
    encoding = sys.stdout.encoding
    target = sys.argv[1]
    if target == "post":
        profile = profiles[sys.argv[2]]
        thread = sys.argv[3]
        text = unicode(sys.argv[4], encoding)
        print text
        task = create_post_task(thread, text, profile)
        print "Create task %s" % task.id
    elif target == "thread":
        profile = profiles[sys.argv[2]]
        forum = sys.argv[3]
        subject = unicode(sys.argv[4], encoding)
        text = unicode(sys.argv[5], encoding)
        task = create_thread_task(forum, subject, text, profile)
        print task.id
    elif target == "check":
        print check_task(sys.argv[2])
    elif target == "result":
        print get_task_result(sys.argv[2])
    elif target == "add-admin":
        from contaminant.data.models.user import User
        login, password = sys.argv[2:4]
        user = User.create_new_user(login, password, 1)
        user.activate_hash = None
        user.active = True
        user.save()
    else:
        print "Unknown target, need thread, post, check, result"


if __name__ == "__main__":
    main()
