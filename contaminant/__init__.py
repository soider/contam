# from __future__ import absolute_import

__author__ = 'mihail'

import logging
from . import conf

if conf.DEBUG:
    default_level = logging.DEBUG
else:
    default_level = logging.ERROR

default_formatter = logging.Formatter('%(asctime)s \
- %(name)s \
- %(levelname)s \
- %(message)s')


loggers = {
    "contaminant.cli.tools": {"level": default_level,
                              "handlers": [
    (logging.StreamHandler(),
     default_formatter)
                              ]},
    "contaminant.core.boards": {"level": default_level,
                                "handlers": [
                                    (logging.StreamHandler(),
                                     default_formatter)
                                ]},
    "contaminant.workflow.tasks": {"level": default_level,
                                   "handlers": [

                                   ]},
    "contaminant.data.models": {"level": default_level,
                                "handlers": [
                                    (logging.StreamHandler(),
                                     default_formatter)
                                ]},
    "contaminant.web": {"level": default_level,
                        "handlers": [
    (logging.StreamHandler(),
     default_formatter)
                        ]},
    "contaminant.tools": {"level": default_level,
                          "handlers": [
                              (logging.StreamHandler(),
                               default_formatter)
                          ]},
    "contaminant.web.resources": {"level": default_level,
                                  "handlers": [
                                      # Please, don't use any stream
                                      # handlers for contaminant.web
                                      # because there is default flask handler
                                      #(logging.StreamHandler(),
                                      # default_formatter)
                                  ]},
    "__main__": {"level": default_level,
                 "handlers": [
                     (logging.StreamHandler(),
                      default_formatter)
                 ]}
}

for logger_name, settings in loggers.items():
    logger = logging.getLogger(logger_name)
    for handler, formatter in settings["handlers"]:
        logger.addHandler(handler)
        handler.setFormatter(
            formatter
        )
    logger.setLevel(
        settings.get("level", default_level)
    )

if conf.DEBUG:
    logger = logging.getLogger('grab')
    logger.setLevel(logging.DEBUG)
