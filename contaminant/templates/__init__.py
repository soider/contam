__author__ = 'mihail'

from jinja2 import Environment, PackageLoader

env = Environment(loader=PackageLoader('contaminant', 'templates'))

get_template = env.get_template
