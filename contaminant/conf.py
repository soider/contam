__author__ = 'soider'

DEBUG = True

# Webclient root url
ROOT_URL = "http://127.0.0.1/client"
# API url
API_URL = "http://127.0.0.1:5000"

# If True, will autoconnect on any first import from contaminant.data
AUTO_CONNECT = True

# Mongo requisites
# Syntax as kwargs for mongoengine.connect
MONGO_CONNECTION = {
    "db": "contaminant",
    "host": "127.0.0.1",
    "port": 27017,
    # username
    # password
}


# Any secret, need for salt gen etc
# Also passed to flask.application

SECRET = "b072d20079919840b6f0fb3bdaf98c71"

# Email

FROM_EMAIL = "contaminant@contaminant.local"

SMTP_HOST = "localhost"
SMTP_PORT = 25
SMTP_AUTH = False
STMP_USER = "login"
SMTP_PASS = "password"

# Celery
BROKER_URL = 'amqp://guest@localhost//'

# Where to store tasks result
CELERY_RESULT_BACKEND = "mongodb"

# Credentials
CELERY_MONGODB_BACKEND_SETTINGS = {
    "host": "127.0.0.1",
    "port": 27017,
    "database": "contaminant_celery",
    "taskmeta_collection": "tasks_results",
}

# CELERY_TASK_SERIALIZER = 'json'
# CELERY_RESULT_SERIALIZER = 'json'
CELERY_TIMEZONE = 'Europe/Moscow'
CELERY_ENABLE_UTC = True

CELERY_CHORD_PROPAGATES = True

# Try to override with user settings
# Any module with name user_conf
# in sys.path is more priority
# and can override any settings from this file
# default settings will store in
# contaminant.conf.default_settings
try:
    import user_conf
except ImportError:
    pass
else:
    default_settings = globals()
    from user_conf import *
