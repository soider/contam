__author__ = 'soider'


templates = [{
             "board_class": "PhpBoard",
             "name": "PhpBBBasictemplate",
             "config": {
                 "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
                     "login_form": {
                         "id": "login"
                     },
                 "login_input_xpath": "//input[@name='username']",
                 "new_post_error_message_xpath": "//p[@class='error']",
                 "new_post_link_xpath":
             "//a[text()[contains(., 'Post a reply')]]",
                 "new_post_submit_args": {
                     "submit_name": "post"
                 },
                 "new_post_text_input_xpath": "//textarea[@name='message']",
                 "new_post_wait_on_page": 10,
                 "new_post_wait_on_page_after": 0,
                 "new_thread_error_message_xpath": "//p[@class='error']",
                 "new_thread_link_xpath":
                 "//a[text()[contains(., 'Post a new topic')]]",
                 "new_thread_subject_input_xpath": "//input[@name='subject']",
                 "new_thread_submit_args": {
                     "submit_name": "post"
                 },
                 "new_thread_text_input_xpath": "//textarea[@name='message']",
                 "new_thread_wait_on_page": 10,
                 "new_thread_wait_on_page_after": 10,
                 "password_input_xpath": "//input[@name='password']",
                 "post_form": {
                     "id": "postform"
                 },
                 "root_url": "Need to replace from profile",
                 "login_url": "Need to replace from profile",
                 "thread_form": {
                     "id": "postform"
                 }
             },
             },
             {
                 "board_class": "InvisionDefaultBoard",
                 "name": "InvisionBasicTemplate",
                 "config": {
                     "root_url": "Need to replace from profile",
                     "login_url": "Need to replace from profile",
                     "login_input_xpath": "//input[@name='ips_username']",
                     "password_input_xpath": "//input[@name='ips_password']",
                     "login_form": {
                         "id": "login"
                     },
                     "post_form": {
                         "id": "postingform"
                     },
                     "thread_form": {
                         "id": "postingform"
                     },
                     "forum_link_xpath_format": "//a[@title='{0}']",
                     "new_thread_wait_on_page_after": 3,
                     "new_thread_link_xpath":
                     "//a[text()[contains(., 'Start New Topic')]]",
                     "new_thread_text_input_xpath": "//textarea[@name='Post']",
                     "new_thread_subject_input_xpath":
                     "//input[@name='TopicTitle']",
                     "new_thread_error_message_xpath":
                     "//p[@class='message error']",
                     "new_post_link_xpath": "//a[text()\
[contains(., 'Reply to this topic')]]",
                     "new_post_text_input_xpath": "//textarea[@name='Post']",
                     "new_post_error_message_xpath":
                     "//p[@class='message error']",
                 }
             },
             {
                 "board_class": "Vbulletin4Board",
                 "name": "VBulletin4Basic",
                 "config": {
                     "root_url": "Need to replace from profile",
                     "login_url": "Need to replace from profile",
                     "login_input_xpath": "//input[@name='vb_login_username']",
                     "password_input_xpath":
                     "//*[@name='vb_login_md5password']",
                     "vbulletin_unauthorized_element":
                     "//input[@class='loginbutton']",
                     "login_form": {
                         "id": "navbar_loginform"
                     },
                     "post_form": {
                         "name": "vbform"
                     },
                     "thread_form": {
                         "name": "vbform"
                     },
                     "forum_link_xpath_format":
                     "//a[text()[contains(., '{0}')]]",
                     "new_thread_link_xpath": "//a[text()\
[contains(., 'Post New Thread')]]",
                     "new_thread_text_input_xpath":
                     "//textarea[@name='message']",
                     "new_thread_subject_input_xpath":
                     "//input[@name='subject']",
                     "new_thread_submit_args": {
                         "submit_name": "sbutton"
                     },
                     "new_thread_wait_on_page": 10,
                     "new_thread_wait_on_page_after": 10,
                     "new_thread_error_message_xpath":
"//div[@class='blockbody errorblock'] | //*[@class='standard_error']",

                     "new_post_link_xpath":
                     "//a[text()[contains(., 'Reply to Thread')]]",
                     "new_post_wait_on_page": 10,
                     "new_post_wait_on_page_after": 0,
                     "new_post_text_input_xpath":
                     "//textarea[@name='message']",
                     "new_post_submit_args": {
                         "submit_name": "sbutton"
                     },
                     "new_post_error_message_xpath":
"//div[@class='blockbody errorblock'] | //*[@class='standard_error']",
                 }
             }]
