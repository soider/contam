__author__ = 'soider'

from mongoengine import Document
from bson.objectid import ObjectId
from mongoengine.fields import DBRef
from contaminant.tools.decorators import inject_logger

from mongoengine import connect
from contaminant import conf

if conf.AUTO_CONNECT:
    connect(**conf.MONGO_CONNECTION)


class BaseModel(Document):

    meta = {
        'abstract': True
    }

    @inject_logger
    def __init__(self, logger=None, *args, **values):
        self.logger = logger
        super(BaseModel, self).__init__(*args, **values)

    def set_ref(self, field, collection, id):
        setattr(self,
                field,
                DBRef(collection, ObjectId(id)))

    def update_from(self, **kwargs):
        new_kwargs = dict(
            (
                ("set__{0}".format(key), value)
                for (key, value) in kwargs.items()
                if key not in ('self',
                               'safe_update',
                               'upsert',
                               'multi',
                               'write_options')
            )
        )
        return self.update(**new_kwargs)

    def set_user_by_id(self, user_oid):
        """
        Set dbref to user_oid
        """
        return self.set_ref("user", "user", user_oid)
