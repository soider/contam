__author__ = 'soider'

import datetime

from mongoengine import fields
from mongoengine.queryset import QuerySet
from bson.objectid import ObjectId

from contaminant.data.models import BaseModel
from contaminant.data.models.user import User


class CookieQuerySet(QuerySet):

    def find_by_user_and_domain(self, user_id, domain):
        """
        Find all documents with specified
        `user_id` and `domain` fields

        :param user_id mongo uuid
        :param domain string

        :return users
        """
        user_id = ObjectId(user_id)
        return self.filter(user=user_id, domain=domain)


class Cookie(BaseModel):

    meta = {'queryset_class': CookieQuerySet}

    user = fields.ReferenceField(User, dbref=False, required=True)
    domain = fields.StringField(required=True)
    cookies = fields.DictField()
    date_modified = fields.DateTimeField(default=datetime.datetime.now)

    @classmethod
    def find_or_create_by_user_and_domain(cls, user, domain):
        """
        Create or find document with specified fields

        :param user mongo uuid
        :param domain string

        :return Cookie instance
        """
        try:
            return cls.objects.find_by_user_and_domain(user, domain)[0]
        except IndexError:
            return cls()
