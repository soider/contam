__author__ = 'soider'

import datetime

from mongoengine import fields
from mongoengine.queryset import QuerySet
from bson.objectid import ObjectId, InvalidId

from contaminant.data.models import BaseModel
from contaminant.data.models.user import User

from contaminant.workflow.broker import celery
from celery.task import Task as CeleryTask


class TaskQuerySet(QuerySet):

    def find_by_user(self, user_id):
        """
        Find all documents with specified
        `user_id` and `domain` fields

        :param user_id mongo uuid

        :return tasks
        """
        user_id = ObjectId(user_id)
        return self.filter(user=user_id)


class Task(BaseModel):

        meta = {'queryset_class': TaskQuerySet}
        _result_cache = None

        user = fields.ReferenceField(User, dbref=False, required=True)
        celery_id = fields.StringField(required=True)

        def get_status(self):
            self.logger.debug("GETTING TASK RESULT %s" % self.celery_id)
            res = celery.AsyncResult(self.celery_id)
            self._result_cache = res
            return res.status

        def get_result(self):
            if self._result_cache is not None:
                return self._result_cache.result
            else:
                res = celery.AsyncResult(self.celery_id)
                return res.result

        def get_error(self):
            if self._result_cache is not None:
                res = self._result_cache.result
            else:
                res = celery.AsyncResult(self.celery_id)
            try:
                return str(res.args[0])
            except IndexError:
                return str(res)

        def get_description(self):
            status = self.get_status()
            desc = {"id": str(self.id),
                    "status": status}
            if status == "SUCCESS":
                desc["result"] = self.get_result()
            elif status == "FAILURE":
                desc["error"] = self.get_error()
            return desc

        @classmethod
        def find_by_user_and_id(cls, user_id, task_id):
            try:
                return cls.objects.\
                    find_by_user(user_id).\
                    filter(id=ObjectId(task_id))[0]
            except (IndexError, InvalidId):
                raise ValueError("No such task")
