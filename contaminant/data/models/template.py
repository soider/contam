__author__ = 'soider'

from mongoengine import fields

from contaminant.data.models import BaseModel


class Template(BaseModel):

    name = fields.StringField(required=True, unique=True)
    config = fields.DictField(required=True)
    board_class = fields.StringField(required=True)
