__author__ = 'soider'

import datetime

from mongoengine import Document, fields
from mongoengine.queryset import QuerySet

from bson.objectid import ObjectId
from bson.objectid import InvalidId

from contaminant.data.models import BaseModel
from contaminant.data.models.user import User
from contaminant.data.models.template import Template


class ByUserQuerySet(QuerySet):

    def by_user(self, user_id):
        """
        Find all documents with specified
        user
        :param user_id mongo uuid
        :return users
        """
        user_id = ObjectId(user_id)
        return self.filter(user=user_id)


class Profile(BaseModel):
    meta = {'queryset_class': ByUserQuerySet}
    user = fields.ReferenceField(User, dbref=False, required=True)
    date_created = fields.DateTimeField(default=datetime.datetime.now)
    login = fields.StringField(required=True)
    password = fields.StringField(required=True)
    template = fields.ReferenceField(Template, dbref=False, required=True)
    url = fields.URLField(required=True)
    login_url = fields.URLField(required=True)
    name = fields.StringField()

    def set_template(self, template_id):
        """
        Set dbref to template
        :param template_id: template oid

        """
        return self.set_ref("template", "template", template_id)

    @classmethod
    def find_by_id(cls, id):
        try:
            return cls.objects.filter(id=ObjectId(id))[0]
        except (IndexError, InvalidId):
            raise ValueError("No such object")

    @classmethod
    def find_by_id_and_user(cls, id, user_id):
        try:
            return cls.objects.filter(
                id=ObjectId(id),
                user=ObjectId(user_id)
            )[0]
        except (IndexError, InvalidId):
            raise ValueError("No such object")

    def get_profile(self):
        config = dict(self.template.config.items())
        config["root_url"] = self.url
        config["login_url"] = self.login_url
        return {
            "login": self.login,
            "password": self.password,
            "url": self.url,
            "board_class": self.template.board_class,
            "config": config,
            "user_id": str(self.user.id)
        }
