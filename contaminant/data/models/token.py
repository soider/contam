__author__ = 'soider'

import datetime
import uuid
import hashlib

from mongoengine import fields

from contaminant.conf import SECRET

from contaminant.data.models import BaseModel
from contaminant.data.models.user import User


def default_expire_at():
    """Helper for default expire_at value"""
    return datetime.datetime.now() +\
        datetime.timedelta(days=1)


class Token(BaseModel):
    """Access-tokens document"""

    token = fields.StringField(required=True)
    user = fields.ReferenceField(User, dbref=False, required=True)
    expire_at = fields.DateTimeField(required=True,
                                     default=default_expire_at)

    @classmethod
    def create_token(cls, user):
        """
        Create new access token

        :param user - User instance

        :return Token instance
        """
        token = cls()
        token.set_ref("user", "user", user.id)
        uid = uuid.uuid4()
        token.token = str(uid) + hashlib.md5(str(uid) + SECRET).hexdigest()
        token.save()
        return token

    @classmethod
    def delete_user_tokens(cls, user):
        """Delete every user token

        :param user User instance"""
        for token in cls.objects.filter(user=user.id):
            token.delete()

    @classmethod
    def is_valid_token(cls, token_string):
        """
        Check that there is token with token_string value
        and this token isn't expired

        :param token_string string
        :return boolean
        """
        try:
            token = cls.objects.filter(token=token_string)[0]
        except IndexError:
            return False
        if token.is_expired():
            token.delete()
            return False
        return True

    @classmethod
    def find_by_token(cls, token_string):
        """Find all tokens with value
        :param token_string string"""
        return cls.objects.filter(token=token_string)

    def is_expired(self):
        """
        Check that instance is expired or not
        :return boolean"""
        return not self.expire_at > datetime.datetime.now()
