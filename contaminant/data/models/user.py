__author__ = 'soider'

import random
import time
import hashlib


from mongoengine import fields
from mongoengine.queryset import QuerySet, NotUniqueError

from contaminant.data.models import BaseModel
from contaminant.data.errors import NoSuchUser


class UserQuerySet(QuerySet):

    """QuerySet for User"""

    def by_login(self, login):
        try:
            return self.filter(login=login)[0]
        except IndexError:
            raise NoSuchUser("No such user", login)

    def find_by_hash(self, hash):
        try:
            return self.filter(activate_hash=hash, active=False)
        except IndexError:
            raise NoSuchUser("No such user")


class User(BaseModel):

    """User mapping"""

    meta = {'queryset_class': UserQuerySet}

    login = fields.EmailField(required=True, unique=True)
    role = fields.IntField(required=True, default=0)
    salt = fields.StringField(required=True)
    password_hash = fields.StringField(required=True)
    active = fields.BooleanField(default=False, required=True)
    activate_hash = fields.StringField()

    @classmethod
    def create_new_user(cls, login, password, role=0):
        """
        Create and return new user model instance
        Doesn't save in mongo

        :param login string email
        :param password string password
        :return new_user"""

        salt = cls.create_salt(login, password)
        password_hash = cls.create_password_hash(
            salt, login, password
        )
        new_user = cls()
        new_user.password_hash = password_hash
        new_user.login = login
        new_user.salt = salt
        new_user.role = role
        new_user.active = False
        new_user.activate_hash = new_user.create_salt("activate", login)
        return new_user

    @classmethod
    def create_new_user_and_save(cls, login, password, role=0):
        user = cls.create_new_user(login, password, role)
        try:
            user.save()
        except NotUniqueError:
            raise ValueError("There is user with such login")
        return user

    @classmethod
    def create_salt(cls, str1, str2):
        """
        Create and return random salt based on two string

        :param str1 begin string
        :param str2 end string
        :return salt string
        """
        salt = None
        salt = str1 * random.randint(0, 100)
        salt += str2 * random.randint(0, 100)
        salt += str(time.time())
        salt = hashlib.md5(salt).hexdigest()
        return salt

    @classmethod
    def create_password_hash(cls, salt, login, password):
        """
        Create hash for salt, login, password

        :param salt string
        :param login string
        :param password string

        :return md5 hex"""
        return hashlib.md5(
            "".join([salt, login, password, salt])
        ).hexdigest()

    @classmethod
    def activate_by_hash(cls, hash):
        try:
            user = cls.objects.find_by_hash(hash)[0]
        except NoSuchUser:
            raise ValueError("Invalid hash")
        user.activate()

    def is_valid_password(self, password):
        """
        Check is valid password for current instance

        :param password string

        :return bool
        """
        hash = self.create_password_hash(
            self.salt,
            self.login,
            password
        )
        if hash != self.password_hash:
            self.logger.warning("Invalid password %s %s",
                                self.login,
                                password)
            return False
        return True

    def activate(self):
        self.active = True
        self.activate_hash = None
        self.save()
