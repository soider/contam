# *-* encoding: utf8 *-*
__author__ = 'soider'

login_url_ipb = "http://a39829.demo.invisionpower.com/index.php\
?app=core&module=global&section=login"

root_url_vbul = "http://084ad0f5-8463-4374-9b1f-142328510563.\
sitebuilder.vbulletin.com/"

login_url_vbul = "http://084ad0f5-8463-4374-9b1f-142328510563.\
sitebuilder.vbulletin.com/auth/login-form"

profiles = {
    "ipb": {
        "board_class": "InvisionDefaultBoard",
        "login": "admin",
        "password": "IPUZruRW",
        "user_id": "517e5126fe01d68514e24749",
        "config": {
            "root_url": "http://a39829.demo.invisionpower.com",
            "login_url": login_url_ipb,
            "login_input_xpath": "//input[@name='ips_username']",
            "password_input_xpath": "//input[@name='ips_password']",
            "login_form": {
                "id": "login"
            },
            "post_form": {
                "id": "postingform"
            },
            "thread_form": {
                "id": "postingform"
            },
            "forum_link_xpath_format": "//a[@title='{0}']",
            "new_thread_wait_on_page_after": 3,
            "new_thread_link_xpath": "//a[text()[contains(., \
'Start New Topic')]]",
            "new_thread_text_input_xpath": "//textarea[@name='Post']",
            "new_thread_subject_input_xpath": "//input[@name='TopicTitle']",
            "new_thread_error_message_xpath": "//p[@class='message error']",
            "new_post_link_xpath": "//a[text()\
[contains(., 'Reply to this topic')]]",
            "new_post_text_input_xpath": "//textarea[@name='Post']",
            "new_post_error_message_xpath": "//p[@class='message error']",
        }
    },
    "vbul": {
        "board_class": "Vbulletin4Board",
        "login": "soider    ",
        "password": "spb43afk",
        "user_id": "517e5126fe01d68514e24749",
        "config": {
            "root_url": "http://192.168.1.71/vbul/forum.php",
            "login_url": "http://192.168.1.71/vbul/forum.php",
            "login_input_xpath": "//input[@name='vb_login_username']",
            "password_input_xpath": "//*[@name='vb_login_md5password']",
            "vbulletin_unauthorized_element": "//input[@class='loginbutton']",
            "login_form": {
                "id": "navbar_loginform"
            },
            "post_form": {
                "name": "vbform"
            },
            "thread_form": {
                "name": "vbform"
            },
            "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
            "new_thread_link_xpath": "//a[text()\
[contains(., 'Post New Thread')]]",
            "new_thread_text_input_xpath": "//textarea[@name='message']",
            "new_thread_subject_input_xpath": "//input[@name='subject']",
            "new_thread_submit_args": {
                "submit_name": "sbutton"
            },
            "new_thread_wait_on_page": 10,
            "new_thread_wait_on_page_after": 10,
            "new_thread_error_message_xpath":
    "//div[@class='blockbody errorblock'] | //*[@class='standard_error']",
            "new_post_link_xpath":
    u"//a[text()[contains(., 'Ответить в теме')]]",
            "new_post_wait_on_page": 10,
            "new_post_wait_on_page_after": 0,
            "new_post_text_input_xpath": "//textarea[@name='message']",
            "new_post_submit_args": {
                "submit_name": "sbutton"
            },
            "new_post_error_message_xpath":
    "//div[@class='blockbody errorblock'] | //*[@class='standard_error']",
        }
    },
    "phpbb": {
        "board_class": "PhpBoard",
        "login": "soider",
        "password": "spb43afk",
        "user_id": "517e5126fe01d68514e24749",
        "config": {
            "root_url": "http://192.168.1.71/forum/",
            "login_url": "http://192.168.1.71/forum/ucp.php?mode=login",
            "login_input_xpath": "//input[@name='username']",
            "password_input_xpath": "//input[@name='password']",
            "login_form": {
                "id": "login"
            },
            "post_form": {
                "id": "postform"
            },
            "thread_form": {
                "id": "postform"
            },
            "forum_link_xpath_format": "//a[text()[contains(., '{0}')]]",
            "new_thread_link_xpath": "//a[text()\
[contains(., 'Post a new topic')]]",
            "new_thread_text_input_xpath": "//textarea[@name='message']",
            "new_thread_subject_input_xpath": "//input[@name='subject']",
            "new_thread_submit_args": {
                "submit_name": "post"
            },
            "new_thread_wait_on_page": 10,
            "new_thread_wait_on_page_after": 10,
            "new_thread_error_message_xpath": "//p[@class='error']",

            "new_post_link_xpath":
    "//a[text()[contains(., 'Ответить в теме')]]",
            "new_post_wait_on_page": 10,
            "new_post_wait_on_page_after": 0,
            "new_post_text_input_xpath": "//textarea[@name='message']",
            "new_post_submit_args": {
                "submit_name": "post"
            },
            "new_post_error_message_xpath": "//p[@class='error']",
        }
    }
}
